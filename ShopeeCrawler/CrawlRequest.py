class CrawlRequest:
    base_url=""
    category=""
    category_id=""

    def __init__(self, base_url, category, category_id):
        self.base_url = base_url
        self.category = category
        self.category_id = category_id
