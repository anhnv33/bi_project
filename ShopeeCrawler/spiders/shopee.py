import scrapy
import json 
import uuid
import requests
import time

from ShopeeCrawler.CrawlRequest import CrawlRequest


class ShopeeSpider(scrapy.Spider):
    name = 'shopee_product'
    order_number = 0

    def start_requests(self):
        crawlRequests = [  CrawlRequest("https://shopee.vn/Th%E1%BB%9Di-Trang-Nam-cat.11035567?page=",'Thời trang nam',str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C3%81o-Kho%C3%A1c-cat.11035567.11035568?page=",'Áo khoác',str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C3%81o-Vest-v%C3%A0-Blazer-cat.11035567.11035572?page=",'Áo Vest và Blazer',str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C3%81o-Hoodie-%C3%81o-Len-%C3%81o-N%E1%BB%89-cat.11035567.11035578?page=", 'Áo Hoodie, Áo Len & Áo Nỉ', str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Qu%E1%BA%A7n-Jeans-cat.11035567.11035583?page=", 'Quần Jeans', str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/a-cat.11035567.11035584?page=", 'Quần Dài/Quần Âu', str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/a-cat.11035567.11035590?page=", 'Quần Short', str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/a-cat.11035567.11035592?page=", 'Áo', str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C3%81o-Ba-L%E1%BB%97-cat.11035567.11035597?page=", "Áo Ba Lỗ", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C4%90%E1%BB%93-L%C3%B3t-cat.11035567.11035598?page=", "Đồ Lót", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C4%90%E1%BB%93-Ng%E1%BB%A7-cat.11035567.11035603?page=", "Đồ Ngủ", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/%C4%90%E1%BB%93-B%E1%BB%99-cat.11035567.11035604?page=", "Đồ Bộ", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/V%E1%BB%9B-T%E1%BA%A5t-cat.11035567.11035605?page=", "Vớ/Tất", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Trang-Phục-Truyền-Thống-cat.11035567.11035606?page=", "Trang Phục Truyền Thống", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Đồ-Hóa-Trang-cat.11035567.11035611?page=", "Đồ Hóa Trang", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Trang-Ph%E1%BB%A5c-Ng%C3%A0nh-Ngh%E1%BB%81-cat.11035567.11035612?page=", "Trang Phục Ngành Nghề", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Kh%C3%A1c-cat.11035567.11035613?page=", "Khác", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Trang-S%E1%BB%A9c-Nam-cat.11035567.11035614?page=", "Trang Sức Nam", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/K%C3%ADnh-M%E1%BA%AFt-Nam-cat.11035567.11035620?page=", "Kính Mắt Nam", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Th%E1%BA%AFt-L%C6%B0ng-Nam-cat.11035567.11035625?page=", "Thắt Lưng Nam", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/C%C3%A0-v%E1%BA%A1t-N%C6%A1-c%E1%BB%95-cat.11035567.11035626?page=", "Cà vạt & Nơ cổ", str(uuid.uuid1())),
                    CrawlRequest("https://shopee.vn/Ph%E1%BB%A5-Ki%E1%BB%87n-Nam-cat.11035567.11035627?page=", "Phụ Kiện Nam", str(uuid.uuid1()))
                    ]
        for crawlRequest in crawlRequests:
            for i in range(0,99):
                request = scrapy.Request(url = crawlRequest.base_url+str(i), 
                                        callback=self.parse_link)
                request.cb_kwargs['base_url'] = crawlRequest.base_url
                request.cb_kwargs['category'] = crawlRequest.category
                request.cb_kwargs['category_id'] = crawlRequest.category_id

                yield request
    def background_waiting():
        print('waiting for new request')
        time.sleep(1)

    
    def parse_link(self, response, base_url, category, category_id):
        print('Amen')
        yield dict(
                base_url = base_url,
                category = category,
                category_id = category_id,
            )
        
        for product in response.css("div.col-xs-2-4.shopee-search-item-result__item"):
            link = ''
            if product.css("div a::attr(href)").get():
                link = "https://shopee.vn/" + product.css("div a::attr(href)").get()
            product_name = ''
            if product.css("div a div div div._1ObP5d div._1nHzH4 div.PFM7lj div::text").get():
                product_name = product.css("div a div div div._1ObP5d div._1nHzH4 div.PFM7lj div::text").get()
            price = 0
            if product.css("div > a > div > div > div._1ObP5d > div._32hnQt > div.WTFwws._1k2Ulw._5W0f35 > span._24JoLh::text").get():
                price = product.css("div > a > div > div > div._1ObP5d > div._32hnQt > div.WTFwws._1k2Ulw._5W0f35 > span._24JoLh::text").get().replace('.', '')
            price = int(price)
            shop_address = ''
            if product.css("div > a > div > div > div._1ObP5d > div._2CWevj::text").get():
                shop_address = product.css("div > a > div > div > div._1ObP5d > div._2CWevj::text").get()
            img_url = ''
            if product.css("div > a > div > div > div._25_r8I._2SHkSu > img::attr(src)").get():
                img_url = product.css("div > a > div > div > div._25_r8I._2SHkSu > img::attr(src)").get()

            number_sold = 0
            # TODO: per month
            if product.css('div > a > div > div > div._1ObP5d > div.beMRch > div.go5yPW::text').get():
                number_sold = product.css('div > a > div > div > div._1ObP5d > div.beMRch > div.go5yPW::text').get().replace('Đã bán ', '').replace(',', '.').replace('/tháng', '')
                if 'k' in number_sold:
                    number_sold = number_sold.replace('k', '')
                    number_sold = int(float(number_sold) * 1000)
                number_sold = int(number_sold)

            sale_off_percent = 0
            if product.css("div > a > div > div > div._25_r8I._2SHkSu > div._1jPz8l > div > div > span.percent::text").get():
                sale_off_percent = product.css("div > a > div > div > div._25_r8I._2SHkSu > div._1jPz8l > div > div > span.percent::text").get().replace('%', '')
            star_list = product.css("div.shopee-rating-stars__star-wrapper")
            star_rate = 0
            if star_list:
                star_rate = len(star_list)
            channel_sold = 'shopee'
            product_id = str(uuid.uuid1())

            link_api = "http://123.31.38.148:9200/shopee/_doc/" + product_id
            print('link_api', link_api)
            data = {'product_id':product_id, 'product_name': product_name, 'link': link, 'price': price, 'shop_address': shop_address, 'img_url': img_url, 'channel_sold': channel_sold, 'category': category, 'category_id': category_id, 'sale_off_percent': sale_off_percent, 'star_rate': star_rate, 'number_sold': number_sold
            }
            print('data', data)
            response = requests.post(link_api, json = {'product_id':product_id, 'product_name': product_name, 'link': link, 'price': price, 'shop_address': shop_address, 'img_url': img_url, 'channel_sold': channel_sold, 'category': category, 'category_id': category_id, 'sale_off_percent': sale_off_percent, 'star_rate': star_rate, 'number_sold': number_sold
            })
            print('response_api', response)
