import scrapy
import json 
import uuid

class ShopeeCategorySpider(scrapy.Spider):
    name = 'shopee_category'
    order_number = 0
    def start_requests(self):
        base_url = "https://shopee.vn/"
        yield scrapy.Request(url = base_url, callback=self.parse_link)
    def parse_link(self, response):
        for product in response.css("div.col-xs-2-4.shopee-search-item-result__item"):
            link = "https://shopee.vn/" + product.css("div a::attr(href)").get()
            product_name = product.css("div a div div div._1ObP5d div._1nHzH4 div.PFM7lj div::text").get()
            price = product.css("div > a > div > div > div._1ObP5d > div._32hnQt > div.WTFwws._1k2Ulw._5W0f35 > span._24JoLh::text").get()
            shop_address = product.css("div > a > div > div > div._1ObP5d > div._2CWevj::text").get()
            img_url = product.css("div > a > div > div > div._25_r8I._2SHkSu > img::attr(src)").get()
            channel_sold = 'shopee'
            print('link product: ', link)
            print('product_name: ', product_name)
            print('product id: ', uuid.uuid1())
            print('price', price)
            print('shop_address', shop_address)
            print('img_url', img_url)
            print('channel_sold', channel_sold)