from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from ShopeeCrawler.spiders.shopee import ShopeeSpider
 
 
process = CrawlerProcess(get_project_settings())
process.crawl(ShopeeSpider)
process.start()